# Single_lever_paddle_3D

Repository containing the source files of the 3D printed single lever paddle based on the design of IU1OPK - using microswitches as  contacts.
First prototype was wired as sideswiper (cootie) and works as expected, some small improvements are on the way - work is in progress!!
